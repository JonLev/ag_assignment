# README #

Allan Gray Twitter Feed Assignment

## Description ##
This application displays a Twitter like feed, given a .txt file containing users, and who they're following; and a txt file containing the users' tweets.

## How to execute ##
Execute the python file "Main.py". Follow the instructions which will prompt a user (.txt file containing users and who they follow) and tweet (.txt file containing the tweets of the users) files to be entered (either their full path and name or just the name if the .txt files are stored in the same folder as the Main.py.

I have included the user.txt and tweet.txt files that I used, which can be found under the downloads section on BitBucket. These are merely extended from the .txt files supplied.

Jonathan Levenson