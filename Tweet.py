__author__ = "Jonathan Levenson"
'''
This is the tweet object. Each tweet that is created contains:
> A name - who it was tweeted by
> The tweet - contents of the tweet
> An ID - the time stamp of when the tweet was made
'''
class Tweet:
    def __init__(self, name, tweet,ID):
        self.name = name #Who the tweet belongs to
        self.tweet = tweet #The tweet itself
        self.ID = ID #the time stamp (in this case from the file)

    def getTweet (self):
        return self.tweet

    def getName (self):
        return self.name

    def getID (self):
        return self.ID