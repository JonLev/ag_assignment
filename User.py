__author__ = "Jonathan Levenson"
'''
This is the user object. Each user contains:
> A name - the name of the user
> A list of tweets - The list of tweet objects which have been made by this user
> A list of followers - The list of follower's user names who this user follows
'''
class User:
    def __init__(self, name):
        self.name = name #Sets the objects name
        self.tweets = [] #Stores a list of tweet objects
        self.following = [] #Creates an empty array to store who the user is following
        self.addFollower(self.name) #Sets user to follow themself (by default)

    def addTweet(self, tweet): #adds a tweet
        self.tweets.append(tweet) #Appends it to the tweet list

    def addFollower(self, usrName): #Adds the followers username to the following list
        self.following.append(usrName)

    def getName (self): #returns the users name
        return self.name

    def getFollowers(self): #returns list of users being followed by user
        return self.following

    def getTweet(self,ID): #returns individual tweet based on the ID supplied
        return self.tweets[str(ID)].getTweet()

    def getTweets(self): #returns a list of all the user's tweets
            return self.tweets