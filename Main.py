__author__ = "Jonathan Levenson"
'''
ASSUMPTIONS MADE:
1) No new users will be found in the tweet.txt file. (I.e. All new users will be found in the
    users.txt file)
2) User names are unique. No two users can have the same user name.
3) The txt files given will follow the format supplied. There will be no errors in formatting by
    the time it gets to the twitter feed application
4) Nobody's name will contain the keyword " follows " or ", ". In reality, delimiters would be
    more complex, and non english words, to ensure it would not occur in the data string.
5) Tweets are limited to 140 characters. All tweets within the tweet txt file will be less than
    140 characters.
'''

from User import *
from Tweet import *

def readUsersTxt (fileName):
    try:
        f = open(fileName, "r") #Opens the user txt file in order to build user database
        lineCount = 0 #keeps track of lines in file for exception reporting
        for line in f: #Reads each line of user txt file
            if (line.strip() == ""):#Checks line isn't blank
                continue

            try:
                usr = line.split (" follows ") #Splits string on "follows" to separate the user
                                                # from who they are following
                usr[1] = usr[1].strip ("\n") #strips the new line from the remaining piece of data
                if (usr[0] not in data): #checks if the user is in the data base
                    data[usr[0]] = User(usr[0]) #if they aren't, adds them

                following = usr[1].split(", ") #splits remaining data on a ', ' (the people being
                                                                                    # followed)
                for entry in following: #loops through various followed users (comma seperated
                                                                                        # values)
                    if (entry != ""): #Makes sure that there is a username present
                        if (entry not in data): #checks to see if the followed user is in the
                                                                                    # user database
                            data[entry] = User(entry) #adds the users to the database if they
                                                                            # aren't in it already

                        if (entry not in data[usr[0]].getFollowers()): #checks whether followed
                                                                        # user is already on the
                                                                        # users following list
                            data[usr[0]].addFollower(entry) #if not adds them to the following list

                lineCount = lineCount + 1

            except IndexError:#Catches the index out of bounds error caused if there is a problem
                                                    # with splitting on the " follows " delimiter

                print ("Format error in line " + str(lineCount) + " of " + fileName + " file")#
                continue #skips that iteration of the loop so that the corrupt data will not be
                                                                        # added to the database

        print (fileName + " successfully imported")
        f.close() #closes the file reader

    except IOError: #catches the exception if the txt file is unable to open
        print ("Unable to open file: " + fileName)
        readUsersTxt(userFileIn())

def readTweetTxt(fileName):
    try:
        f = open(fileName, "r") #Opens the tweets txt file in order to add tweets to users
        lineCount = 0 #keeps track of lines in file (the time stamp)
        for line in f: #Reads each line of tweet txt file
            if (line.strip() == ""):  # Checks line isn't blank
                continue
            try:
                usr = line[:line.find("> ")] #Did not use .split becaues "> " may be used in a
                                                            # tweet, and then it would be split on
                tweet = line[line.find("> ")+2:]
                tweet = tweet.strip ("\n") #Strips the new line from the string
                tempTweet = Tweet(usr,tweet,lineCount)
                data[usr].addTweet (tempTweet)#Adds the tweet to the user objects tweets, as well
                                                            #as the code (identifier code lineCount)
                lineCount = lineCount + 1

            except KeyError:
                print ("Format error in line " + str (lineCount) + " of " + fileName + " file")
                continue

        print (fileName + " successfully imported")
        f.close()

    except IOError:
        print ("Unable to open file: " + fileName)
        readTweetTxt(tweetFileIn())

def printFeed():#Prints out the feed
    for usr in sorted(data):#Sorts the list of users to display feed alphabetically
        tweetList = [] #The list of tweets that apply to users based on who they follow
        print (usr + "\n") #Prints the users name (whose feed it is)
        for follower in (data[usr].getFollowers()): #gets the list of who the user is following
            tweetList.extend(data[follower].getTweets()) #Adds followers tweets to list of tweets to be displayed

        for sort in sorted(tweetList, key=lambda x: x.getID()): #sorts the list of tweets based on chronological order (ID)
            print ("\t@" + sort.getName() + ": "+ sort.getTweet()+"\n") #prints them out


def userFileIn():
    userFile = raw_input ("Please enter the name of the user file, followed by '.txt'\n e.g. "
                          "'user.txt'")
    while (userFile.find(".txt")== -1 or userFile.strip() == ""):
        userFile = raw_input("You have entered an invalid file name\nPlease enter the name of the"
                             " user file, followed by '.txt'\n e.g. 'user.txt'")

    print ("userFile name = " + userFile)
    return userFile

def tweetFileIn():
    tweetFile = raw_input ("Please enter the name of the tweet file, followed by '.txt'\n e.g. "
                           "'tweet.txt'")
    while (tweetFile.find(".txt")== -1 or tweetFile.strip() == ""):
        tweetFile = raw_input("You have entered an invalid file name\nPlease enter the name of the"
                              " tweet file, followed by '.txt'\n e.g. 'tweet.txt'")

    print ("userFile name = " + tweetFile)
    return tweetFile



while (True):
    data = {}  # The main dictionary (hash table) which is used to store the user objects
    readUsersTxt(userFileIn())
    readTweetTxt(tweetFileIn())
    print
    printFeed()

    if (raw_input("If you would like to re-run using different file names press enter, otherwise "
                  "(q)uit") != ""):
        break
